#!/bin/bash

SRPM='custom-kernel-5.8.15-*.src.rpm'
RPM='custom-kernel-5.8.15-*.x86_64.rpm'

echo ""
echo "======== Build SRPM ========"
echo ""
sleep 3

rm src/custom-config.tar.xz
tar -cJf src/custom-config.tar.xz custom-config

mock --buildsrpm --spec custom-kernel.spec --sources src/

rm ./${SRPM}
cp /var/lib/mock/fedora-33-x86_64/result/${SRPM} .

echo ""
echo "======== Build RPM ========="
echo ""
sleep 3

mock --rebuild ./${SRPM}

rm ./${RPM}
cp /var/lib/mock/fedora-33-x86_64/result/${RPM} .
