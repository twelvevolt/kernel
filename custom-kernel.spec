%global debug_package %{nil}

%define local_version custom

Summary:       The Linux kernel with custom config
Name:          custom-kernel
License:       GPLv2
URL:           https://www.kernel.org/
Version:       5.8.15
Release:       6
ExclusiveArch: x86_64

BuildRequires: binutils
BuildRequires: gcc, gcc-c++, make
BuildRequires: flex, bison, bc
BuildRequires: hostname, kmod
BuildRequires: openssl-devel, openssl
BuildRequires: elfutils-libelf-devel
BuildRequires: perl
BuildRequires: dwarves

Requires(post): dracut, grubby
Requires(post): xz

Requires(postun): grubby

Source0: linux-%{version}.tar.xz
Source1: custom-config.tar.xz

%description
The kernel package contains the Linux kernel (vmlinuz), the core of
any Linux operating system. The kernel handles the basic functions of
the operating system: memory allocation, process allocation, device
input and output, etc.
This package uses custom kernel configuration file.

%prep
%setup -q -n linux-%{version} -a1

%build
%define postfix %{release}.%{local_version}.%{_arch}

%{__make} mrproper

# Prepare configuration file.
%{__awk} '/^CONFIG_LOCALVERSION=/ { $0 = "CONFIG_LOCALVERSION=\"-%{postfix}\"" } 1' custom-config > .config
%{__make} olddefconfig

%{__make}

%install
### Kernel.
%{__mkdir_p} %{buildroot}/boot
%{__make} INSTALL_PATH=%{buildroot}/boot install
# Rename installed files.
%{__mv} %{buildroot}/boot/vmlinuz %{buildroot}/boot/vmlinuz-%{version}-%{postfix}
%{__mv} %{buildroot}/boot/System.map %{buildroot}/boot/System.map-%{version}-%{postfix}

### Modules
%{__make} INSTALL_MOD_PATH=%{buildroot} modules_install
# Remove broken symlinks
%{__rm} %{buildroot}/lib/modules/%{version}-%{postfix}/build
%{__rm} %{buildroot}/lib/modules/%{version}-%{postfix}/source

%files
%defattr(-,root,root)
%attr(755,root,root)/boot/vmlinuz-%{version}-%{postfix}
%attr(600,root,root)/boot/System.map-%{version}-%{postfix}
%dir /lib/modules/%{version}-%{postfix}
/lib/modules/%{version}-%{postfix}/*

%post
dracut --kver %{version}-%{postfix} --xz
grubby --add-kernel /boot/vmlinuz-%{version}-%{postfix} --initrd /boot/initramfs-%{version}-%{postfix}.img --title "Fedora (%{version}-%{postfix}) %{?fedora}"

%postun
%{__rm} /boot/initramfs-%{version}-%{postfix}.img
grubby --remove-kernel /boot/vmlinuz-%{version}-%{postfix}

%changelog
* Sun Aug 29 2021 twelvevolt <twelvevolt@yandex.ru> [5.8.15-6]
- Disable kernel debug.

* Thu Aug 26 2021 twelvevolt <twelvevolt@yandex.ru> [5.8.15-5]
- Now it is enough to download the desired linux tarball and set the
  appropriate kernel version in spec file.

* Sun Aug 22 2021 twelvevolt <twelvevolt@yandex.ru> [5.8.15-4]
- Add xz compression to initram image.
- Set Kernel compression to xz.
- Enable /proc/config.gz.

* Fri Aug 20 2021 twelvevolt <twelvevolt@yandex.ru> [5.8.15-3]
- Replace tiny config with config-5.8.15-301.fc33.x86_64.

* Thu Aug 19 2021 twelvevolt <twelvevolt@yandex.ru> [5.8.15-2]
- Add architecture information to name.
- Add initramfs generation and deletion.
- Add add/remove grub menue.

* Tue Aug 17 2021 twelvevolt <twelvevolt@yandex.ru> [5.8.15-1]
- Replace make tinyconfig with config file.
- Enable file system cyrillic module.
- Remove broken symlinks (warning: absolute symlink).
- Set local version string to custom.
- Add local version to vmlinuz and System.map name.

* Mon Aug 16 2021 twelvevolt <twelvevolt@yandex.ru> [5.8.15-0]
- Set initial minimal tiny build configuration.
- Set debug_package to nil.
- Change attributes for vmlinuz (755), System.map (600).
